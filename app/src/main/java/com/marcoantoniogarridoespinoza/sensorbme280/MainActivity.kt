package com.marcoantoniogarridoespinoza.sensorbme280

import androidx.appcompat.app.AppCompatActivity
import android.graphics.Bitmap
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebViewClient




class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        val myWebView: WebView = findViewById(R.id.webview)
        myWebView.settings.javaScriptEnabled = true

        myWebView.webViewClient = WebViewClient()
        val settings = myWebView.settings


        myWebView.loadUrl("https://sensorbme280marcoantonio.000webhostapp.com/esp-chart.php")
       // myWebView.loadUrl( "javascript:window.location.reload( true )" );


        myWebView.webChromeClient = object : WebChromeClient() {

        }
        myWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                resquest: WebResourceRequest?
            ): Boolean {
                return false
            }


            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                myWebView.reload()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                myWebView.reload()
            }
        }


    }
}
